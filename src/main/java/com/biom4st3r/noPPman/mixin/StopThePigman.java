package com.biom4st3r.noPPman.mixin;

import net.minecraft.block.PortalBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PortalBlock.class)

public class StopThePigman {
	@Inject(at = @At("HEAD"), method = "onScheduledTick",cancellable = true)
	private void init(CallbackInfo info) {
		info.cancel();
	}
}
